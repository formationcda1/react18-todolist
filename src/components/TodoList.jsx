import PropTypes from "prop-types";
import TodoItem from "./TodoItem";

function TodoList(props) {
  const { todos } = props;
  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul>
        {/* Pour chaques tâches de mon tableau affiche là
         et affiche si elle est complètée */}
        {/* Affichage de chaque tâche avec TodoItem */}
        {todos.map((todo) => (
          <TodoItem key={todos.id}{...todo} />
          )
         )
        }
      </ul>
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
